En este apartado realizaremos todo el despliegue de los 5 puntos antes mencionados. Simulando 3 sensores lora como si estuviesen conectados a sus respectivos sensores. Veremos paso a paso como en el localhost podemos desplegar los contenedores docker de todos los servicios antes mencionados: ChirpStack y de Mainflux. Además desplegar Grafana y también InfluxDB. Simular 3 nodos, y poder ver dicha información armando un Dashboard de visualización.

## Instalación:

Lo primero es clonar el repositorio en nuestro sistema. Docker y docker-compose son requisitos previos.

~~~
git clone https://github.com/mainflux/mainflux.git
~~~

## Despliegue de Mainflux
Entramos a la carpeta mainflux y corremos:
~~~
docker-compose -f docker/docker-compose.yml up
~~~

Esto abrirá todos los contenedores acoplables Mainflux y los interconectará en la composición. En este ejemplo usaremos InfluxDB como base de datos de mensajes. Por lo tanto, debemos ejecutar la composición de Docker del complemento `influxdb-writer.` Para eso, ejecute estos comandos desde la raíz del proyecto:
~~~
docker-compose -f docker/addons/influxdb-writer/docker-compose up
~~~

## Ejecutar Servidor de Red
Antes de ejecutar lora-adapter debe instalar y ejecutar ChirpStack. Primero, ejecute el siguiente comando:
~~~
git clone git clone https://github.com/brocaar/chirpstack-docker.git
~~~

Una vez que todo esté descargado, ejecute el siguiente comando desde la raíz del proyecto del servidor ChirpStack:
~~~
docker-compose up
~~~

Mainflux y ChirpStack utilizan sus propios brokers MQTT. Por defecto, esos usan el puerto MQTT estándar 1883. Si está ejecutando ambos sistemas en la misma máquina, debe usar puertos diferentes. Puede arreglar esto en el lado de Mainflux configurando la variable de entorno `MF_MQTT_ADAPTER_PORT`.

## Configuración del servidor de Red
Ahora que ambos sistemas se están ejecutando, se debe aprovisionar el servidor ChirpStack, que ofrece integración con servicios externos, una API RESTful y gRPC. La GUI del servidor ofrece una interfaz gráfica para administrar el sistema.
En este punto, suponemos ya que nuestro Gateway está enviando mensajes al servidor LoRa. O sea que nuestro Gateway físico de LoRa está enviando paquetes UDP al IP del LoRa-Gateway-Bridge (donde se ejecuta el servidor LoRa) en el puerto 1700.

## Entonces es momento de:

**Agregar un servidor de redes**
Como cada contenedor tiene su propio nombre de host, debe utilizar el nombre de host del contenedor `networkserver`  al agregar el servidor de red en la interfaz web del servidor de aplicaciones ChirpStack.

Cuando se utiliza el ejemplo anterior, significa que se debe ingresar `chirpstack-network-server:8000` como nombre de host del servidor de red.

![](img/networkserver.png)


**Crear una organización:** para agregar sus propias puertas de enlace a la red, debe tener una organización.

![](img/organization.png)

**Crear un perfil de puertas de enlace:** en este perfil puede seleccionar los canales de radio LoRa y el servidor de red LoRa para usar.

![](img/profilegateway.png)

**Crear un perfil de servicio:** un perfil de servicio conecta una organización a un servidor de red y define las características que una organización puede usar en este servidor de red.

![](img/profileservice.png)

**Crear una puerta de enlace:** debe establecer la identificación adecuada para que LoRa Server la descubra.

![](img/gateway.png)
El gateway será creado y accesible desde la página de GATEWAYS:

![](img/gateway2.png)

Si se ha encontrado el Gateway, debería ver algo así:

![](img/gateway3.png)

**Crear una aplicación de servidor LoRa:** esto le permitirá crear dispositivos conectándolos a esta aplicación. Es el equivalente de Mainflux de conectar dispositivos a canales.

![](img/application.png)

![](img/application2.png)

**Crear un perfil de dispositivo:** antes de crear un dispositivo, debe crear un perfil de dispositivo donde definirá algún parámetro como la versión MAC de LoRaWAN (formato de la dirección del dispositivo) y el parámetro regional de LoRaWAN (banda de frecuencia). Esto le permitirá crear muchos dispositivos usando este perfil.

![](img/device-profile.png)

**Crear un dispositivo:** finalmente, puede crear un dispositivo. Edite los nombres de los perfiles, seleccione un perfil de dispositivo y genere su clave de dispositivo:

![](img/dispostivo.png)

**Active su dispositivo:** debe configurar el network session key y application session key de su dispositivo. Puede generarlos y copiarlos en la configuración de su dispositivo o puede usar sus propias claves pregeneradas y configurarlas usando la GUI del servidor de aplicaciones LoRa. El dispositivo se conecta a través de OTAA. Asegúrese de que el perfil de dispositivo de LoRa Server esté usando la misma versión que el dispositivo. Si la versión de MAC es 1.0.X application key = app_key y app_eui = deviceEUI. Si la versión de MAC es 1.1 o ABP, se necesitarán ambos parámetros, APP_key y Network key.
