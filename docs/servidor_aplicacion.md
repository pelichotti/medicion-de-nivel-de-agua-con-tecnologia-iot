## Esquema General
Existen muchas plataformas IoT disponibles para nuestros desarrollos, para nuestro caso de estudio se explicará el esquema de la plataforma IoT Mainflux. También de desarrollo open source, escrita en GO.
El diagrama general de conexión desde el sensor hasta el software de aplicación es el siguiente:

![](img/diagrama.png)
